package ru.vmaksimenkov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

@Component
public final class BackupLoadListener extends AbstractDataListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Load data from backup";
    }

    @NotNull
    @Override
    public String command() {
        return "backup-load";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@backupLoadListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[BACKUP LOAD]");
        adminEndpoint.loadBackup(sessionService.getSession());
    }

}
