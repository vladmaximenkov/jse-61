package ru.vmaksimenkov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

@Component
public final class DataJsonSaveFasterXmlListener extends AbstractDataListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Save data to json file";
    }

    @NotNull
    @Override
    public String command() {
        return "data-json-save";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonSaveFasterXmlListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA JSON SAVE]");
        adminEndpoint.saveJson(sessionService.getSession());
    }

}
