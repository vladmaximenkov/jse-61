package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vmaksimenkov.tm.endpoint.ProjectEndpoint;
import ru.vmaksimenkov.tm.endpoint.ProjectRecord;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.listener.AbstractListener;
import ru.vmaksimenkov.tm.service.SessionService;

import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    protected SessionService sessionService;

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    protected void showProject(@Nullable final ProjectRecord project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        if (project.getStatus() != null)
            System.out.println("STATUS: " + project.getStatus());
        System.out.println("CREATED: " + project.getCreated());
        System.out.println("STARTED: " + project.getDateStart());
        System.out.println("FINISHED: " + project.getDateFinish());
        System.out.print(dashedLine());
    }

}
