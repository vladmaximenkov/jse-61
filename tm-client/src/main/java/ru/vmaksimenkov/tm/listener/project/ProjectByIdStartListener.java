package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class ProjectByIdStartListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by id";
    }

    @NotNull
    @Override
    public String command() {
        return "project-start-by-id";
    }

    @Override
    @EventListener(condition = "@projectByIdStartListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        projectEndpoint.startProjectById(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
