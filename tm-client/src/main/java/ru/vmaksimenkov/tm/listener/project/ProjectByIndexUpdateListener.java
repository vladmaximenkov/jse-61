package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Component
public final class ProjectByIndexUpdateListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index";
    }

    @NotNull
    @Override
    public String command() {
        return "project-update-by-index";
    }

    @Override
    @EventListener(condition = "@projectByIndexUpdateListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final int index = TerminalUtil.nextNumber();
        if (!checkIndex(index, projectEndpoint.countProject(sessionService.getSession())))
            throw new IndexIncorrectException();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        projectEndpoint.updateProjectByIndex(sessionService.getSession(), index, name, TerminalUtil.nextLine());
    }

}
