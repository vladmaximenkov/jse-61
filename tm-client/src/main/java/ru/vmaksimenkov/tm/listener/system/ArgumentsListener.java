package ru.vmaksimenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.listener.AbstractListener;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Component
public final class ArgumentsListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Override
    public String argument() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all arguments";
    }

    @NotNull
    @Override
    public String command() {
        return "arguments";
    }

    @Override
    @EventListener(condition = "@argumentsListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractListener listener : listeners) {
            if (!isEmpty(listener.argument())) System.out.println(listener.argument());
        }
    }

}
