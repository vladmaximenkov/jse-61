package ru.vmaksimenkov.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.listener.AbstractListener;
import ru.vmaksimenkov.tm.service.PropertyService;

@Component
public final class VersionListener extends AbstractListener {

    @NotNull
    @Autowired
    protected PropertyService propertyService;

    @NotNull
    @Override
    public String argument() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version";
    }

    @NotNull
    @Override
    public String command() {
        return "version";
    }

    @Override
    @EventListener(condition = "@versionListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println(propertyService.getApplicationVersion());
        System.out.println("[BUILD]");
        System.out.println(Manifests.read("build"));
    }

}
