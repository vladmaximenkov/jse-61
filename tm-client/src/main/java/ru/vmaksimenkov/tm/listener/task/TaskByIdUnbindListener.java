package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIdUnbindListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task by id";
    }

    @NotNull
    @Override
    public String command() {
        return "task-unbind-by-id";
    }

    @Override
    @EventListener(condition = "@taskByIdUnbindListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UNBIND TASK BY ID]");
        if (taskEndpoint.countTask(sessionService.getSession()) < 1)
            throw new TaskNotFoundException();
        System.out.println("ENTER TASK ID:");
        taskEndpoint.unbindTaskById(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
