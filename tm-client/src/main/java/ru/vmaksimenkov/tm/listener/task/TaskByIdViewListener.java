package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIdViewListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "View task by id";
    }

    @NotNull
    @Override
    public String command() {
        return "task-view-by-id";
    }

    @Override
    @EventListener(condition = "@taskByIdViewListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        showTask(taskEndpoint.findTaskById(sessionService.getSession(), TerminalUtil.nextLine()));
    }

}
