package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIndexRemoveListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index";
    }

    @NotNull
    @Override
    public String command() {
        return "task-remove-by-index";
    }

    @Override
    @EventListener(condition = "@taskByIndexRemoveListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        taskEndpoint.removeTaskByIndex(sessionService.getSession(), TerminalUtil.nextNumber());
    }

}
