package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.Status;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskByNameSetStatusListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Set task status by name";
    }

    @NotNull
    @Override
    public String command() {
        return "task-set-status-by-name";
    }

    @Override
    @EventListener(condition = "@taskByNameSetStatusListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (!taskEndpoint.existsTaskByName(sessionService.getSession(), name))
            throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        taskEndpoint.setTaskStatusByName(sessionService.getSession(), name, Status.valueOf(TerminalUtil.nextLine()));
    }

}
