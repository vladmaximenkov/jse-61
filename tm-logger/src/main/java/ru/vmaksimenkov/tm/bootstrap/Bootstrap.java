package ru.vmaksimenkov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.listener.LogMessageListener;
import ru.vmaksimenkov.tm.service.ActiveMQConnectionService;

import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import static ru.vmaksimenkov.tm.constant.Const.SUBJECT;

public class Bootstrap implements Runnable {

    @NotNull
    private final ActiveMQConnectionService connectionService = new ActiveMQConnectionService();

    @Override
    @SneakyThrows
    public void run() {
        @NotNull final Session session = connectionService.getConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(SUBJECT);
        @NotNull final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(new LogMessageListener());
    }

}
