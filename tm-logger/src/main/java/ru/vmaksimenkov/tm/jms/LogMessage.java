package ru.vmaksimenkov.tm.jms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.EventType;

import java.io.Serializable;
import java.util.Date;

@Getter
@AllArgsConstructor
public class LogMessage implements Serializable {

    @Nullable
    private final String type;

    @Nullable
    private final EventType event;

    @Nullable
    private final String json;

    @Nullable
    private final Date date;

}
