package ru.vmaksimenkov.tm.exception.user;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email is already in use...");
    }

}
