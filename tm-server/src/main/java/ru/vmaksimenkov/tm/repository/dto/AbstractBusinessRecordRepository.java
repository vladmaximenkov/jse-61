package ru.vmaksimenkov.tm.repository.dto;

import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityRecord;

public interface AbstractBusinessRecordRepository<E extends AbstractBusinessEntityRecord> extends AbstractRecordRepository<E> {

}