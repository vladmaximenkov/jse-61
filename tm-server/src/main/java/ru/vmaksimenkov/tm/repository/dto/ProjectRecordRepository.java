package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

import java.util.List;

public interface ProjectRecordRepository extends AbstractBusinessRecordRepository<ProjectRecord> {

    @Modifying
    @Query("DELETE FROM ProjectRecord WHERE id = :userId AND name = :name")
    Long deleteByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @Query("SELECT id FROM ProjectRecord WHERE userId = :userId AND name = :name")
    @Nullable String getIdByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    boolean existsByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @Nullable ProjectRecord findByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    void deleteByUserId(@NotNull @Param("userId") String userId);

    void deleteByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    boolean existsByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable List<ProjectRecord> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable ProjectRecord findByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull Long countByUserId(@NotNull @Param("userId") String userId);

}
