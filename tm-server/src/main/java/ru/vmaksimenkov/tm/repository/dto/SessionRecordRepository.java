package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.query.Param;
import ru.vmaksimenkov.tm.dto.SessionRecord;

import java.util.List;

public interface SessionRecordRepository extends AbstractBusinessRecordRepository<SessionRecord> {

    void deleteByUserId(@NotNull @Param("userId") String userId);

    void deleteByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    boolean existsByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable List<SessionRecord> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable SessionRecord findByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull Long countByUserId(@NotNull @Param("userId") String userId);

}
