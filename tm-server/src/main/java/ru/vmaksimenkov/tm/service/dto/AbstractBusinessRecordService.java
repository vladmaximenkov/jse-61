package ru.vmaksimenkov.tm.service.dto;

import ru.vmaksimenkov.tm.api.service.dto.IAbstractBusinessRecordService;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityRecord;
import ru.vmaksimenkov.tm.repository.dto.AbstractBusinessRecordRepository;

public abstract class AbstractBusinessRecordService<E extends AbstractBusinessEntityRecord, R extends AbstractBusinessRecordRepository<E>> extends AbstractRecordService<E, R> implements IAbstractBusinessRecordService<E> {


}