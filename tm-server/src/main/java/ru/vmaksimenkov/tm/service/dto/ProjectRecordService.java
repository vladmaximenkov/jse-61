package ru.vmaksimenkov.tm.service.dto;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.dto.IProjectRecordService;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;

import javax.transaction.Transactional;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
@Transactional
public class ProjectRecordService extends AbstractBusinessRecordService<ProjectRecord, ProjectRecordRepository> implements IProjectRecordService {

    @NotNull
    @Autowired
    private IProjectRecordService service;

    @NotNull
    @Autowired
    private ProjectRecordRepository repository;

    @Override
    public ProjectRecord add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final ProjectRecord project = new ProjectRecord();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        service.add(project);
        return project;
    }

    @Override
    @Nullable
    public ProjectRecord findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return null;
        return repository.findAllByUserId(userId).get(index - 1);
    }

    @Override
    @Nullable
    public String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return null;
        return service.findByIndex(userId, index).getId();
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return;
        @Nullable final String id = service.getIdByIndex(userId, index);
        if (isEmpty(id)) return;
        repository.deleteByUserIdAndId(userId, id);
    }

    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId) || isEmpty(name)) return false;
        return repository.existsByUserIdAndName(userId, name);
    }

    @Override
    @Nullable
    public ProjectRecord findByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId) || isEmpty(name)) return null;
        return repository.findByUserIdAndName(userId, name);
    }

    @Override
    public void finishProjectById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final ProjectRecord project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        service.update(project);
    }

    @Override
    public void finishProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final ProjectRecord project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        service.update(project);
    }

    @Override
    public void finishProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        service.update(project);
    }

    @Override
    public void setProjectStatusById(@NotNull final String userId, @Nullable final String id, @NotNull final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final ProjectRecord project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        service.update(project);
    }

    @Override
    public void setProjectStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final ProjectRecord project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        service.update(project);
    }

    @Override
    public void setProjectStatusByName(@NotNull final String userId, @Nullable final String name, @NotNull final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        service.update(project);
    }

    @Override
    public void startProjectById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final ProjectRecord project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        service.update(project);
    }

    @Override
    public void startProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectRecord project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        service.update(project);
    }

    @Override
    public void startProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        service.update(project);
    }

    @Override
    public void updateProjectById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        service.update(project);
    }

    @Override
    public void updateProjectByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByIndex(userId, index - 1);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        service.update(project);
    }

    @Override
    public void updateProjectByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String nameNew,
            @Nullable final String description
    ) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        service.update(project);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) return;
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return false;
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<ProjectRecord> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) return null;
        return repository.findAllByUserId(userId);
    }

    @Override
    @Nullable
    public ProjectRecord findById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return null;
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return;
        repository.deleteByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        if (isEmpty(userId)) return 0L;
        return repository.countByUserId(userId);
    }

    @NotNull
    public List<ProjectRecord> findAll() {
        return Lists.newArrayList(repository.findAll());
    }

    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Nullable
    @Transactional
    public ProjectRecord add(@Nullable final ProjectRecord entity) {
        if (entity == null) return null;
        return repository.save(entity);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        assert id != null;
        return repository.existsById(id);
    }

    @Override
    @Nullable
    public ProjectRecord findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public void add(@Nullable final List<ProjectRecord> entities) {
        if (entities == null || entities.isEmpty()) return;
        repository.saveAll(entities);
    }

    @Transactional
    public void update(@NotNull final ProjectRecord entity) {
        repository.save(entity);
    }

    @Transactional
    public void remove(@Nullable final ProjectRecord entity) {
        if (entity == null) return;
        repository.delete(entity);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) return;
        repository.deleteById(id);
    }

    @NotNull
    @Override
    public Long size() {
        return repository.count();
    }

}
