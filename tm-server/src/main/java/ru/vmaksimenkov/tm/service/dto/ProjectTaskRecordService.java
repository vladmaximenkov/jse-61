package ru.vmaksimenkov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.dto.IProjectTaskRecordService;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;

import javax.transaction.Transactional;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
@Transactional
public class ProjectTaskRecordService implements IProjectTaskRecordService {

    @NotNull
    @Autowired
    private ProjectRecordRepository projectRepository;

    @NotNull
    @Autowired
    private TaskRecordRepository taskRepository;

    @Override
    @Transactional
    public void bindTaskByProjectId(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId)) return;
        if (!projectRepository.existsByUserIdAndId(userId, projectId)) throw new ProjectNotFoundException();
        taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
    }

    @Override
    @Transactional
    public void clearTasks(@NotNull final String userId) {
        taskRepository.deleteByUserIdAndProjectIdIsNotNull(userId);
        projectRepository.deleteByUserId(userId);
    }

    @Nullable
    @Override
    public List<TaskRecord> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) return;
        if (!projectRepository.existsByUserIdAndId(userId, projectId)) throw new ProjectNotFoundException();
        taskRepository.deleteByUserIdAndProjectId(userId, projectId);
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void removeProjectByIndex(@NotNull final String userId, @NotNull final Integer projectIndex) {
        if (!checkIndex(projectIndex, projectRepository.countByUserId(userId))) throw new IndexIncorrectException();
        @Nullable final String projectId = projectRepository.findAllByUserId(userId).get(projectIndex - 1).getId();
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.deleteByUserIdAndProjectId(userId, projectId);
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        if (isEmpty(projectName)) return;
        @Nullable final String projectId = projectRepository.getIdByUserIdAndName(userId, projectName);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.deleteByUserIdAndProjectId(userId, projectId);
        projectRepository.deleteByUserIdAndName(userId, projectName);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmpty(taskId)) return;
        @Nullable TaskRecord task = taskRepository.findByUserIdAndId(userId, taskId);
        if (task == null) return;
        task.setProjectId(null);
        taskRepository.save(task);
    }

}
