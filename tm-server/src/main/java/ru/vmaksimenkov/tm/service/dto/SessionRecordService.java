package ru.vmaksimenkov.tm.service.dto;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.user.AccessDeniedException;
import ru.vmaksimenkov.tm.repository.dto.SessionRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;
import ru.vmaksimenkov.tm.util.HashUtil;
import ru.vmaksimenkov.tm.util.SignatureUtil;

import javax.transaction.Transactional;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
@Transactional
public class SessionRecordService extends AbstractBusinessRecordService<SessionRecord, SessionRecordRepository> implements ISessionRecordService {

    @NotNull
    @Autowired
    private ISessionRecordService service;

    @NotNull
    @Autowired
    private SessionRecordRepository repository;

    @NotNull
    @Autowired
    private UserRecordRepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        @Nullable final UserRecord user = userRepository.findByLogin(login);
        if (user == null) return false;
        if (user.isLocked()) return false;
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @Nullable
    public SessionRecord findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return null;
        return repository.findAllByUserId(userId).get(index - 1);
    }

    @Override
    @Nullable
    public String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return null;
        return service.findByIndex(userId, index).getId();
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return;
        @Nullable final String id = service.getIdByIndex(userId, index);
        if (isEmpty(id)) return;
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void close(@NotNull final SessionRecord session) {
        service.validate(session);
        repository.delete(session);
    }

    @Override
    public void closeAll(@NotNull final SessionRecord session) {
        service.validate(session);
        repository.deleteByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public List<SessionRecord> getListSession(@NotNull final SessionRecord session) {
        service.validate(session);
        return repository.findAllByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public UserRecord getUser(@NotNull final SessionRecord session) {
        @NotNull final String userId = getUserId(session);
        return userRepository.findById(userId).orElse(null);
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final SessionRecord session) {
        service.validate(session);
        return session.getUserId();
    }

    @Override
    public boolean isValid(@NotNull final SessionRecord session) {
        try {
            service.validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    public SessionRecord open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check || isEmpty(login)) throw new AccessDeniedException();
        @Nullable final UserRecord user = userRepository.findByLogin(login);
        if (user == null) return null;
        @NotNull final SessionRecord session = new SessionRecord();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        service.add(session);
        return service.sign(session);
    }

    @Nullable
    @Override
    public SessionRecord sign(@Nullable final SessionRecord session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        final int cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        service.update(session);
        return session;
    }

    @Override
    public void validate(@NotNull final SessionRecord session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        service.validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserRecord user = userRepository.findById(userId).orElse(null);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final SessionRecord session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        @Nullable final SessionRecord temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionRecord sessionSign = sign(temp);
        if (sessionSign == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!repository.existsById(session.getId())) throw new AccessDeniedException();
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) return;
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return false;
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<SessionRecord> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) return null;
        return repository.findAllByUserId(userId);
    }

    @Override
    @Nullable
    public SessionRecord findById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return null;
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return;
        repository.deleteByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        if (isEmpty(userId)) return 0L;
        return repository.countByUserId(userId);
    }

    @NotNull
    public List<SessionRecord> findAll() {
        return Lists.newArrayList(repository.findAll());
    }

    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Nullable
    @Transactional
    public SessionRecord add(@Nullable final SessionRecord entity) {
        if (entity == null) return null;
        return repository.save(entity);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        assert id != null;
        return repository.existsById(id);
    }

    @Override
    @Nullable
    public SessionRecord findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void add(@Nullable final List<SessionRecord> entities) {
        if (entities == null || entities.isEmpty()) return;
        repository.saveAll(entities);
    }

    @Transactional
    public void update(@NotNull final SessionRecord entity) {
        repository.save(entity);
    }

    @Transactional
    public void remove(@Nullable final SessionRecord entity) {
        if (entity == null) return;
        repository.delete(entity);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) return;
        repository.deleteById(id);
    }

    @NotNull
    @Override
    public Long size() {
        return repository.count();
    }

}
