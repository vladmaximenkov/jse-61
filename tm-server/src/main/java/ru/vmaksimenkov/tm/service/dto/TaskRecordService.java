package ru.vmaksimenkov.tm.service.dto;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.dto.ITaskRecordService;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
@Transactional
public class TaskRecordService extends AbstractBusinessRecordService<TaskRecord, TaskRecordRepository> implements ITaskRecordService {

    @NotNull
    @Autowired
    private ITaskRecordService service;

    @NotNull
    @Autowired
    private TaskRecordRepository repository;

    @Override
    public TaskRecord add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final TaskRecord task = new TaskRecord();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        service.add(task);
        return task;
    }

    @Override
    @Nullable
    public TaskRecord findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return null;
        return repository.findAllByUserId(userId).get(index - 1);
    }

    @Override
    @Nullable
    public String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return null;
        return service.findByIndex(userId, index).getId();
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId) || index == null || !checkIndex(index, service.size(userId))) return;
        @Nullable final String id = service.getIdByIndex(userId, index);
        if (isEmpty(id)) return;
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) return false;
        return repository.existsByUserIdAndName(userId, name);
    }

    @Nullable
    public List<TaskRecord> findAll(@NotNull final String userId, @NotNull final Comparator<TaskRecord> comparator) {
        return repository.findAllByUserId(userId);
    }

    public @Nullable TaskRecord findByName(@NotNull final String userId, @NotNull final String name) {
        return repository.findByUserIdAndName(userId, name);
    }

    @Override
    public void finishTaskById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final TaskRecord task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        service.update(task);
    }

    @Override
    public void finishTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final TaskRecord task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        service.update(task);
    }

    @Override
    public void finishTaskByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final TaskRecord task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        service.update(task);
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        service.remove(findByName(userId, name));
    }

    @Override
    public void setTaskStatusById(@NotNull final String userId, @Nullable final String id, @NotNull final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final TaskRecord task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        service.update(task);
    }

    @Override
    public void setTaskStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final TaskRecord task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        service.update(task);
    }

    @Override
    public void setTaskStatusByName(@NotNull final String userId, @Nullable final String name, @NotNull final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final TaskRecord task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        service.update(task);
    }

    @Override
    public void startTaskById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final TaskRecord task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        service.update(task);
    }

    @Override
    public void startTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final TaskRecord task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        service.update(task);
    }

    @Override
    public void startTaskByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final TaskRecord task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        service.update(task);
    }

    @Override
    public void updateTaskById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final TaskRecord task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        service.update(task);
    }

    @Override
    public void updateTaskByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final TaskRecord task = findByIndex(userId, index - 1);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        service.update(task);
    }

    @Override
    public void updateTaskByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String nameNew,
            @Nullable final String description
    ) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        @Nullable final TaskRecord task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setName(nameNew);
        task.setDescription(description);
        service.update(task);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) return;
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return false;
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<TaskRecord> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) return null;
        return repository.findAllByUserId(userId);
    }

    @Override
    @Nullable
    public TaskRecord findById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return null;
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (isEmpty(userId) || isEmpty(id)) return;
        repository.deleteByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        if (isEmpty(userId)) return 0L;
        return repository.countByUserId(userId);
    }

    @NotNull
    public List<TaskRecord> findAll() {
        return Lists.newArrayList(repository.findAll());
    }

    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Nullable
    @Transactional
    public TaskRecord add(@Nullable final TaskRecord entity) {
        if (entity == null) return null;
        return repository.save(entity);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        assert id != null;
        return repository.existsById(id);
    }

    @Override
    @Nullable
    public TaskRecord findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void add(@Nullable final List<TaskRecord> entities) {
        if (entities == null || entities.isEmpty()) return;
        repository.saveAll(entities);
    }

    @Transactional
    public void update(@NotNull final TaskRecord entity) {
        repository.save(entity);
    }

    @Override
    @Transactional
    public void remove(@Nullable final TaskRecord entity) {
        if (entity == null) return;
        repository.delete(entity);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) return;
        repository.deleteById(id);
    }

    @NotNull
    @Override
    public Long size() {
        return repository.count();
    }

}
