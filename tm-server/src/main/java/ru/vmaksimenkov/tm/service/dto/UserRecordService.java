package ru.vmaksimenkov.tm.service.dto;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.api.service.dto.IUserRecordService;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.empty.EmptyEmailException;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyLoginException;
import ru.vmaksimenkov.tm.exception.empty.EmptyPasswordException;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.exception.user.EmailExistsException;
import ru.vmaksimenkov.tm.exception.user.LoginExistsException;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;
import ru.vmaksimenkov.tm.util.HashUtil;

import javax.transaction.Transactional;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
@Transactional
public class UserRecordService extends AbstractRecordService<UserRecord, UserRecordRepository> implements IUserRecordService {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserRecordService service;

    @NotNull
    @Autowired
    private UserRecordRepository repository;

    @NotNull
    public UserRecord create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (existsByLogin(login)) throw new LoginExistsException();
        if (existsByEmail(email)) throw new EmailExistsException();
        @NotNull final UserRecord user = new UserRecord();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        service.add(user);
        return user;
    }

    @NotNull
    public UserRecord create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (existsByLogin(login)) throw new LoginExistsException();
        @NotNull final UserRecord user = new UserRecord();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        service.add(user);
        return user;
    }

    public boolean existsByEmail(@Nullable final String email) {
        if (isEmpty(email)) return false;
        return repository.existsUserRecordByEmail(email);
    }

    public boolean existsByLogin(@Nullable final String login) {
        if (isEmpty(login)) return false;
        return repository.existsUserRecordByLogin(login);
    }

    @Nullable
    public UserRecord findByLogin(@Nullable final String login) {
        return repository.findByLogin(login);
    }

    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final UserRecord user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        service.update(user);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        service.remove(findByLogin(login));
    }

    public void setPassword(@NotNull final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final UserRecord user = repository.findById(userId).orElse(null);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        service.update(user);
    }

    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final UserRecord user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        service.update(user);
    }

    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        @Nullable final UserRecord user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        service.update(user);
    }

    @NotNull
    public List<UserRecord> findAll() {
        return Lists.newArrayList(repository.findAll());
    }

    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Nullable
    @Transactional
    public UserRecord add(@Nullable final UserRecord entity) {
        if (entity == null) return null;
        return repository.save(entity);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        assert id != null;
        return repository.existsById(id);
    }

    @Override
    @Nullable
    public UserRecord findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void add(@Nullable final List<UserRecord> entities) {
        if (entities == null || entities.isEmpty()) return;
        repository.saveAll(entities);
    }

    @Transactional
    public void update(@NotNull final UserRecord entity) {
        repository.save(entity);
    }

    @Override
    @Transactional
    public void remove(@Nullable final UserRecord entity) {
        if (entity == null) return;
        repository.delete(entity);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) return;
        repository.deleteById(id);
    }

    @NotNull
    @Override
    public Long size() {
        return repository.count();
    }

}
